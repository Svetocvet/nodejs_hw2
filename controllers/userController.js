const User = require('../models/User');
const { BAD_REQUEST_CODE, INTERNAL_SERVER_ERROR_CODE} = require('../config/default');

module.exports.getUser = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.user._id});
        if (!user) {
            return res.status(BAD_REQUEST_CODE).json({message: "Requested profile doesn't exist"});
        }
        return res.json({user: req.user});
    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not get profile, some error happened'});
    }
};

module.exports.deleteUser = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.user._id});
        if (!user) {
            return res.status(BAD_REQUEST_CODE).json({message: "Requested profile doesn't exist"});
        } else {
            await User.findOneAndDelete({_id: req.user._id});
            return res.json({message: 'Success'});
        }

    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not delete profile, some error happened'});
    }
};

module.exports.changePassword = async (req, res) => {
    try {
        if (!req.body.oldPassword || !req.body.newPassword) {
            return res.status(BAD_REQUEST_CODE).json({message: 'Invalid credentials'});
        }
        let user = await User.findOne({_id: req.user._id});
        if (!user) {
            return res.status(BAD_REQUEST_CODE).json({message: "Requested profile doesn't exist"});
        }
        if (req.body.oldPassword !== user.password) {
            return res.status(BAD_REQUEST_CODE).json({message: "Wrong old password"});
        }
        await User.findOneAndUpdate({_id: req.user._id}, {password: req.body.newPassword});
        return res.json({message: 'Success'});
    } catch (err) {
        return res.status(INTERNAL_SERVER_ERROR_CODE).json({message: 'Could not update password, some error happened'});
    }
};
