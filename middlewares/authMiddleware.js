const jwt = require('jsonwebtoken');
const config = require('../config/default');
const {BAD_REQUEST_CODE, INTERNAL_SERVER_ERROR_CODE} = require('../config/default');

module.exports = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    if (!authHeader) {
        return res.status(BAD_REQUEST_CODE).json({status: 'No authorization header found'});
    }
    const [, jwtToken] = authHeader.split(' ');
    try {
        req.user = jwt.verify(jwtToken, config.secret);
        next();
    } catch (err) {
        return res.status(BAD_REQUEST_CODE).json({status: 'Invalid JWT'});
    }
};