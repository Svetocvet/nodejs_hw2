const express = require('express');
const router = express.Router();

const {getUser, deleteUser, changePassword} = require('../controllers/userController');
const authMiddleware = require('../middlewares/authMiddleware');

router.get('/users/me', authMiddleware, getUser);
router.delete('/users/me', authMiddleware, deleteUser);
router.patch('/users/me', authMiddleware, changePassword);

module.exports = router;