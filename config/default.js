const config = {};
config.db = {};

config.db.name = 'hw2';
config.db.host = 'localhost';
config.db.port = 27017;
config.port = process.env.PORT || 8080;
config.secret = 'someverysecretsecret';

config.BAD_REQUEST_CODE = 400;
config.INTERNAL_SERVER_ERROR_CODE = 500;

module.exports = config;